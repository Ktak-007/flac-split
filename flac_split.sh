#!/bin/bash
#===============================================================================
#
#         FILE:  flac_split.sh
#
#         USAGE:  ./flac_split.sh ./flac_file.{flac,ape,wv} ./flack_file.cue
#===============================================================================

if [ $# -ne 2 ]; then
    echo "Usage: $0 ./flac_file.{flac,ape,wv} ./flack_file.cue"
    exit 1
fi

cuebreakpoints "$2" | shntool split -o flac "$1"
ALBUM=`cat "$2" | awk -F '"' '/^TITLE/{print $2}'`
ARTIST=`cat "$2" | awk -F '"' '/^PERFORMER/{print $2}'`
DIR_READY="$ARTIST/$ALBUM"
mkdir -p "$DIR_READY"

cuetag "$2" split-track*

EXT=`echo $1 | awk -F "." '{print $NF}'`

for f in split-track*; do
    TRK=`metaflac --list $f | awk -F "=" '/TRACKNUMBER/{print $2}'`
    metaflac --list $f |
      awk -F '=' -v c=$TRK -v f="$f" -v dir="$DIR_READY" -v ext=$EXT '/TITLE/{
        printf("mv "f" \""dir"/%02d. "$2"."ext"\"\n", c)
      }'
done | sh
